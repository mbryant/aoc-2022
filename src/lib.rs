pub mod day1 {
    pub type Int = u32;

    pub fn generator(input: &str) -> Vec<Int> {
        input
            .split("\n\n")
            .map(|s| {
                s.lines()
                    .map(|x| x.parse::<Int>().expect("Not an integer"))
                    .sum()
            })
            .collect()
    }

    pub fn part1(input: &[Int]) -> Int {
        *input.iter().max().expect("Must be multipled elves")
    }

    pub fn part2(input: &[Int]) -> Int {
        use itertools::Itertools;

        input
            .iter()
            .map(|&x| -(x as isize))
            .k_smallest(3)
            .map(|x| -x)
            .sum::<isize>() as Int
    }
}

pub mod day2 {
    pub type Int = u32;

    #[derive(PartialEq)]
    pub enum Hand {
        Rock,
        Paper,
        Scissors,
    }

    fn score(hand: &Hand) -> Int {
        // Score is what you played
        match hand {
            Hand::Rock => 1,
            Hand::Paper => 2,
            Hand::Scissors => 3,
        }
    }

    enum End {
        // Points for the outcome
        Lose = 0,
        Draw = 3,
        Win = 6,
    }

    pub fn generator(input: &str) -> Vec<(Hand, Hand)> {
        let bytes = input.as_bytes();

        bytes
            .chunks(4)
            .map(|line| {
                let parse = |c: u8| match c {
                    b'A' | b'X' => Hand::Rock,
                    b'B' | b'Y' => Hand::Paper,
                    _ => Hand::Scissors,
                };

                (parse(line[0]), parse(line[2]))
            })
            .collect()
    }

    pub fn part1(input: &[(Hand, Hand)]) -> Int {
        // Score is what you played plus outcome
        input
            .iter()
            .map(|(them, me)| {
                score(me)
                    + match (them, me) {
                        (x, y) if x == y => End::Draw as Int,
                        (Hand::Rock, Hand::Paper) => End::Win as Int,
                        (Hand::Paper, Hand::Scissors) => End::Win as Int,
                        (Hand::Scissors, Hand::Rock) => End::Win as Int,
                        _ => End::Lose as Int,
                    }
            })
            .sum()
    }

    /// Trivial [Iterator::fold] solution with no real optimization opportunities
    pub fn part2(input: &[(Hand, Hand)]) -> Int {
        input
            .iter()
            .map(|(them, instruction)| match (instruction, them) {
                (Hand::Rock, Hand::Rock) => End::Lose as Int + score(&Hand::Scissors),
                (Hand::Rock, Hand::Paper) => End::Lose as Int + score(&Hand::Rock),
                (Hand::Rock, Hand::Scissors) => End::Lose as Int + score(&Hand::Paper),
                (Hand::Paper, Hand::Rock) => End::Draw as Int + score(them),
                (Hand::Paper, Hand::Paper) => End::Draw as Int + score(them),
                (Hand::Paper, Hand::Scissors) => End::Draw as Int + score(them),
                (Hand::Scissors, Hand::Rock) => End::Win as Int + score(&Hand::Paper),
                (Hand::Scissors, Hand::Paper) => End::Win as Int + score(&Hand::Scissors),
                (Hand::Scissors, Hand::Scissors) => End::Win as Int + score(&Hand::Rock),
            })
            .sum()
    }
}

pub mod day3 {
    pub fn generator(input: &str) -> &str {
        // Parsing doesn't get us anything
        input
    }

    fn priority(c: u8) -> usize {
        (if b'A' <= c && c <= b'Z' {
            27 + (c - b'A')
        } else {
            1 + (c - b'a')
        }) as usize
    }

    pub fn part1(input: &str) -> usize {
        input
            .lines()
            .map(|x| {
                let (l, r) = x.split_at(x.len() / 2);
                let mut each = [false; (b'z' - b'A') as usize + 1];

                for c in l.as_bytes() {
                    each[(c - b'A') as usize] = true;
                }

                let dup = r
                    .as_bytes()
                    .iter()
                    .find(|&c| each[(c - b'A') as usize])
                    .expect("Must be a duplicate");

                priority(*dup)
            })
            .sum()
    }

    pub fn part2(input: &str) -> usize {
        use itertools::Itertools;

        input
            .lines()
            .chunks(3)
            .into_iter()
            .map(|chunk| {
                let each = chunk.enumerate().fold(
                    [0u8; (b'z' - b'A') as usize + 1],
                    |mut each, (i, line)| {
                        for c in line.as_bytes() {
                            each[(c - b'A') as usize] |= 1 << i;
                        }

                        each
                    },
                );

                let (dup, _) = each
                    .iter()
                    .enumerate()
                    .find(|&(_, x)| x == &0x7)
                    .expect("Must be a badge in each group");

                priority(dup as u8 + b'A')
            })
            .sum()
    }
}

pub mod day4 {
    type Int = usize;

    pub struct Range {
        l: Int,
        r: Int,
    }

    pub fn generator(input: &str) -> Vec<(Range, Range)> {
        input
            .lines()
            .map(|line| {
                let (l, r) = line.split_once(',').expect("Must be pairs on each line");

                let l = l.split_once('-').expect("Must be a range");
                let r = r.split_once('-').expect("Must be a range");
                (
                    Range {
                        l: l.0.parse().expect("Invalid integer"),
                        r: l.1.parse().expect("Invalid integer"),
                    },
                    Range {
                        l: r.0.parse().expect("Invalid integer"),
                        r: r.1.parse().expect("Invalid integer"),
                    },
                )
            })
            .collect()
    }

    pub fn part1(input: &[(Range, Range)]) -> usize {
        input
            .iter()
            .filter(|&(l, r)| (l.l <= r.l && l.r >= r.r) || (r.l <= l.l && r.r >= l.r))
            .count()
    }

    pub fn part2(input: &[(Range, Range)]) -> usize {
        input
            .iter()
            .filter(|&(l, r)| l.l <= r.r && l.r >= r.l)
            .count()
    }
}

pub mod day5 {
    const STACKS: usize = 9;
    type Box = u8;
    type Stack = usize;

    pub struct Move {
        count: usize,
        from: Stack,
        to: Stack,
    }

    pub fn generator(input: &str) -> ([Vec<Box>; STACKS], Vec<Move>) {
        // Find where the stack diagram ends
        const TERMINATOR: &str = " 1 ";
        let (stack_region, moves) =
            input.split_at(input.find(TERMINATOR).expect("Must be a list of stacks"));

        // Parse the stacks in chunks of 4 bytes
        let mut stacks: [Vec<Box>; STACKS] = Default::default();
        for (i, chunk) in stack_region.as_bytes().chunks(4).enumerate() {
            let stack = i % STACKS;

            if chunk[0] == b'[' {
                stacks[stack].push(chunk[1]);
            }
        }

        // Flip the stacks so push/pop work as expected
        for stack in stacks.iter_mut() {
            stack.reverse();
        }

        // Parse the moves
        let moves = moves
            .lines()
            .filter(|l| l.starts_with("move"))
            .map(|l| {
                let mut words = l.split(' ');

                Move {
                    count: words
                        .nth(1)
                        .expect("Must be 6 entries")
                        .parse()
                        .expect("Must be integer count"),
                    from: words
                        .nth(1)
                        .expect("Must be 4 entries left")
                        .parse::<Stack>()
                        .expect("Must be integer count")
                        - 1,
                    to: words
                        .nth(1)
                        .expect("Must be 2 entries left")
                        .parse::<Stack>()
                        .expect("Must be integer count")
                        - 1,
                }
            })
            .collect();

        (stacks, moves)
    }

    pub fn part1((stacks, moves): &([Vec<Box>; STACKS], Vec<Move>)) -> String {
        let mut stacks = stacks.clone();

        for m in moves {
            for _ in 0..m.count {
                let mover = stacks[m.from]
                    .pop()
                    .expect("Tried to remove a box that doesn't exist");
                stacks[m.to].push(mover);
            }
        }

        let tops = stacks.map(|s| *s.last().expect("Must be a box on each stack"));
        std::str::from_utf8(&tops)
            .expect("Boxes are only ascii letters")
            .to_string()
    }

    pub fn part2((stacks, moves): &([Vec<Box>; STACKS], Vec<Move>)) -> String {
        let mut stacks = stacks.clone();

        for m in moves {
            let new_len = stacks[m.from].len() - m.count;

            // We effectively want `to.append(from.split_off(new_len))`, but this allocates for no
            // real reason.  We can emulate this behaviour using just slices, but it's a bit
            // complicated since we need to use `split_at_mut` to mutable access two array indexes
            // at the same time (since the compiler doesn't know they'll be different).
            if m.from > m.to {
                let (to, from) = stacks.split_at_mut(m.from);

                let moving = &from[0][new_len..];
                to[m.to].extend(moving);
            } else {
                let (from, to) = stacks.split_at_mut(m.to);

                let moving = &from[m.from][new_len..];
                to[0].extend(moving);
            }

            stacks[m.from].truncate(new_len)
        }

        let tops = stacks.map(|s| *s.last().expect("Must be a box on each stack"));
        std::str::from_utf8(&tops)
            .expect("Boxes are only ascii letters")
            .to_string()
    }
}

pub mod day6 {
    use itertools::Itertools;

    pub fn generator(input: &str) -> &[u8] {
        input.as_bytes()
    }

    pub fn part1(input: &[u8]) -> usize {
        const MESSAGE_SIZE: usize = 4;

        input
            .iter()
            .map(|b| b - b'a')
            .tuple_windows()
            .enumerate()
            .find(|(_, (a, b, c, d))| {
                (1usize << a | 1 << b | 1 << c | 1 << d).count_ones() == (MESSAGE_SIZE as u32)
            })
            .expect("Must be a start marker")
            .0
            + MESSAGE_SIZE
    }

    pub fn part2(input: &[u8]) -> usize {
        const MESSAGE_SIZE: usize = 14;

        input
            .windows(MESSAGE_SIZE)
            .enumerate()
            .find(|(_, window)| {
                window
                    .iter()
                    .map(|b| b - b'a')
                    .fold(0usize, |acc, x| acc | (1 << x))
                    .count_ones()
                    == (MESSAGE_SIZE as u32)
            })
            .expect("Must be a start message")
            .0
            + MESSAGE_SIZE
    }
}

pub mod day7 {
    pub enum Directory {
        Dir(usize, Vec<Directory>),
        File(usize),
    }

    pub fn generator(input: &str) -> Directory {
        fn recurse<'a>(cmds: &mut impl Iterator<Item = &'a str>) -> Directory {
            let mut dir = Vec::new();

            loop {
                // We do this rather than `for cmd in cmds` because that complains about ownership
                // for some reason.
                if let Some(cmd) = cmds.next() {
                    match cmd.as_bytes()[0] {
                        b'c' => {
                            // cd <target>
                            let target = &cmd[3..];

                            if target == ".." {
                                break;
                            } else {
                                dir.push(recurse(cmds));
                            }
                        }
                        _ => {
                            // ls: list of results
                            for entry in cmd.split('\n').skip(1) {
                                match entry.as_bytes()[0] {
                                    b'd' => {}
                                    _ => dir.push(Directory::File(
                                        entry[0..entry.find(' ').expect("Must be a filename")]
                                            .parse()
                                            .expect("Must be a valid file size"),
                                    )),
                                }
                            }
                        }
                    }
                } else {
                    break;
                }
            }

            return Directory::Dir(
                dir.iter()
                    .map(|x| match x {
                        Directory::File(size) => size,
                        Directory::Dir(size, _) => size,
                    })
                    .sum(),
                dir,
            );
        }

        // Skip the first `cd /`
        recurse(&mut input.trim_end().split("\n$ ").skip(1))
    }

    pub fn part1(input: &Directory) -> usize {
        fn recurse(dir: &Directory) -> usize {
            match dir {
                Directory::Dir(size, children) => {
                    return children.iter().map(|dir| recurse(dir)).sum::<usize>()
                        + if *size < 100000 { *size } else { 0 }
                }
                _ => 0,
            }
        }

        recurse(input)
    }

    pub fn part2(input: &Directory) -> usize {
        const TARGET: usize = 30000000;
        const DISK_SIZE: usize = 70000000;

        fn recurse(dir: &Directory, target: usize) -> usize {
            match dir {
                Directory::Dir(size, children) => {
                    let potential_child = children
                        .iter()
                        .map(|c| recurse(c, target))
                        .filter(|&s| s != 0)
                        .min()
                        .unwrap_or(DISK_SIZE);

                    if *size > target {
                        std::cmp::min(*size, potential_child)
                    } else {
                        potential_child
                    }
                }
                _ => 0,
            }
        }

        let target = match input {
            Directory::Dir(full, _) => TARGET - (DISK_SIZE - full),
            _ => unreachable!(),
        };

        recurse(input, target)
    }
}

pub mod day8 {
    const DIM: usize = 99;

    pub fn generator(input: &str) -> [[u8; DIM]; DIM] {
        input
            .lines()
            .map(|x| {
                x.bytes()
                    .map(|b| b - b'0')
                    .collect::<Vec<_>>()
                    .try_into()
                    .expect("Must be DIM trees per row")
            })
            .collect::<Vec<_>>()
            .try_into()
            .expect("Must be DIM rows of trees")
    }

    pub fn part1(input: &[[u8; DIM]; DIM]) -> usize {
        let mut visible = [[false; DIM]; DIM];

        fn visibility(tree: u8, visible: &mut bool, highest: u8) -> Option<u8> {
            if tree > highest {
                *visible = true;

                (tree != 9).then_some(tree)
            } else {
                Some(highest)
            }
        }

        // Each row, starting from the left, then doing a pass from the right
        for (y, row) in input.iter().enumerate() {
            visible[y][0] = true;
            visible[y][DIM - 1] = true;

            row.iter()
                .enumerate()
                .skip(1)
                .try_fold(row[0], |highest, (x, &tree)| {
                    visibility(tree, &mut visible[y][x], highest)
                });
            row.iter()
                .enumerate()
                .rev()
                .skip(1)
                .try_fold(row[DIM - 1], |highest, (x, &tree)| {
                    visibility(tree, &mut visible[y][x], highest)
                });
        }

        // Each column, starting from the top, then doing a pass from the bottom
        for x in 0..DIM {
            visible[0][x] = true;
            visible[DIM - 1][x] = true;

            (1..DIM)
                .map(|y| (y, input[y][x]))
                .try_fold(input[0][x], |highest, (y, tree)| {
                    visibility(tree, &mut visible[y][x], highest)
                });
            (1..DIM)
                .map(|y| (y, input[y][x]))
                .rev()
                .try_fold(input[DIM - 1][x], |highest, (y, tree)| {
                    visibility(tree, &mut visible[y][x], highest)
                });
        }

        visible
            .iter()
            .map(|&row| row.into_iter().filter(|&x| x).count())
            .sum()
    }

    // Super lazy - is there a better approach here?
    pub fn part2(input: &[[u8; DIM]; DIM]) -> usize {
        (0..DIM)
            .map(|y| {
                (0..DIM)
                    .map(|x| {
                        let start = input[y][x];

                        let x1 = (0..x).rev().find(|&x| input[y][x] >= start).unwrap_or(0);
                        let x2 = (x + 1..DIM)
                            .find(|&x| input[y][x] >= start)
                            .unwrap_or(DIM - 1);
                        let y1 = (0..y).rev().find(|&y| input[y][x] >= start).unwrap_or(0);
                        let y2 = (y + 1..DIM)
                            .find(|&y| input[y][x] >= start)
                            .unwrap_or(DIM - 1);

                        (x - x1) * (x2 - x) * (y - y1) * (y2 - y)
                    })
                    .max()
                    .expect("DIM > 0")
            })
            .max()
            .expect("DIM > 0")
    }
}

pub mod day9 {
    #[derive(Debug)]
    pub enum Dir {
        U,
        L,
        R,
        D,
    }

    pub fn generator(input: &str) -> Vec<(Dir, i32)> {
        input
            .lines()
            .map(|line| {
                (
                    match line.as_bytes()[0] {
                        b'U' => Dir::U,
                        b'L' => Dir::L,
                        b'R' => Dir::R,
                        _ => Dir::D,
                    },
                    line[2..].parse().expect("Must have a distance"),
                )
            })
            .collect()
    }

    pub fn part1(input: &[(Dir, i32)]) -> usize {
        use fnv::FnvHashSet;

        let mut locations = FnvHashSet::default();
        let mut head = (0i32, 0i32);
        let mut tail = (0i32, 0i32);

        locations.insert(tail);

        for (dir, dist) in input {
            head = match dir {
                Dir::U => (head.0, head.1 + dist),
                Dir::L => (head.0 - dist, head.1),
                Dir::R => (head.0 + dist, head.1),
                Dir::D => (head.0, head.1 - dist),
            };

            let dist_x = tail.0.abs_diff(head.0);
            let dist_y = tail.1.abs_diff(head.1);

            match (dist_x, dist_y) {
                // Tail doesn't move
                (0, 0) | (0, 1) | (1, 0) | (1, 1) => continue,

                // Tail has to move
                (1, _) | (_, 1) => {
                    // Tail pulled diagonally one, then vertical or horizontal
                    tail = (
                        tail.0 + (head.0 - tail.0).signum(),
                        tail.1 + (head.1 - tail.1).signum(),
                    );

                    locations.insert(tail);
                }
                // At most one direction
                _ => (),
            }

            if tail.0 == head.0 {
                // We moved in the Y direction
                let dist = head.1 - tail.1;

                for _ in 1..dist.abs() {
                    tail = (tail.0, tail.1 + dist.signum() * 1);
                    locations.insert(tail);
                }
            } else {
                // We moved in the X direction
                let dist = head.0 - tail.0;

                for _ in 1..dist.abs() {
                    tail = (tail.0 + dist.signum() * 1, tail.1);
                    locations.insert(tail);
                }
            };
        }

        locations.len()
    }

    pub fn part2(input: &[(Dir, i32)]) -> &str {
        "Left for another day"
    }
}
